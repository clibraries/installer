import os

def getenv(name:str, default:str|None = None, create:bool=False):
    value = os.getenv(name)
    if value is None:
        if create:
            if default is None:
                raise ValueError("Default value is None, cannot create environment variable")
            os.environ[name] = default
            print("Created environment variable %s with value %s"%(name, default))
        return default
    return value