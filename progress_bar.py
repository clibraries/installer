import os

import sys

if(sys.platform == "win32"):
	import ctypes
	from ctypes import wintypes
else:
	import termios

def get_console_width():
	return os.get_terminal_size().columns
def get_console_height():
	return os.get_terminal_size().lines

class console:
	def clear_line():
		print("\033[1E\033[0K", end="")
	def clear():
		print("\033[2J", end="")
	def move_up(n):
		print("\033[%sA"%(n), end="")
	def goto(x, y):
		print("\033[%s;%sH"%(y, x), end="")
	def move_down(n):
		print("\n"*n, end="")

	def save_cursor():
		print("\033[s", end="")
	def restore_cursor():
		print("\033[u", end="")

	

class Bar:
	"""
	Progress bar
	"""
	def __init__(self, total, label=""):
		self.total = total
		self.label = label
		self.progress = 0 #between 0 and total
		len_nb_total = len(str(total))
		len_label = len(label)
		self.reserved_space = (len_nb_total * 2) + 7 + len_label
		
		self.first = True

		self.display()
		
	def display(self):
		if self.first:
			self.first = False
		else:
			console.move_up(1)
		percent = self.progress / self.total
		bar = int(percent * (get_console_width() - self.reserved_space)) # the bar is between 0 and the console width
		print("{0} [{1}{2}] {3}/{4}".format(self.label,"=" * bar, " " * (get_console_width() - self.reserved_space - bar), self.progress, self.total))

	def __iadd__(self, value):
		if self.progress + value > self.total:
			self.progress = self.total
		else:
			self.progress += value
		self.display()
		return self
	
	def __isub__(self, value):
		if self.progress - value < 0:
			self.progress = 0
		else:
			self.progress -= value
		self.display()
		return self
		
	def end_text(self):
		return self.label + " done"
	
	def __str__(self):
		return str(self.progress)
	
	def done(self):
		console.move_up(1)
		print(get_console_width()*" ")
		console.move_up(1)
		print(self.end_text())

	def print(self, *args, **kwargs):
		#print the argument above the progress bar (move up, clear line, print the text, move down, print the bar)
		console.move_up(2)
		console.clear_line()
		print(*args, **kwargs)
		console.move_down(1)
		self.display()


if __name__ == "__main__":
	import time
	bar = Bar(50, "Progress")
	for i in range(50):
		bar += 1
		time.sleep(0.05)
		if i == 25:
			bar.print("Half done")
	bar.done()