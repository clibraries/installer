from threading import Thread as Thread_
from chrono import chrono
from progress_bar import Bar as ProgressBar


class Threaded:

	class Thread(Thread_):
		def __init__(self, target, args, **kwargs):
			super().__init__(**kwargs)
			self.target = target
			self.args = args
			self.result = None

		def run(self):
			self.result = self.target(self, *self.args)

		def get_result(self):
			return self.result
		
		def print(self, *args, **kwargs):
			print(self.str(*args, **kwargs))

		def str(self, *args, **kwargs):
			return "["+self.name+"] "+str(*args, **kwargs)

	class Func:
		"""
		allow to pass arguments to a function without executing it, and to execute it later
		"""
		def __init__(self, func, args : list, print_func : callable = print):
			self.func = func
			self.args = args
			self.result = None

			self.print = print_func

		def __call__(self):
			self.result = self.func(*self.args, self.print)
			return self.result
		
		def get_result(self):
			return self.result
		
		def __str__(self):
			string = str(self.func.__name__)+"("
			for arg in self.args:
				string += type(arg).__name__ + ", "
			string = string[:-2]
			string += ")"
			return string




	def __init__(self,nb_thread, func, args_list, label="", when_done=None, daemon=False):
		"""
		launch the function func in nb_thread threads with the arguments in args_list
		"""
		self.nb_thread = nb_thread
		self.todo = []
		for args in args_list:
			self.todo.append(Threaded.Func(func, args))
		self.results = []
		self.threads = []
		self.ran_by_thread = {"Thread-"+str(i+1):[] for i in range(self.nb_thread)}

		self.exceptions = [] #list of exceptions raised by the threaded functions

		self.label = label

		self.daemon = daemon

		self.bar = ProgressBar(len(self.todo), self.label)
		self.print = self.bar.print #the print function, to print something while the bar is running

		for task in self.todo:
			task.print = self.print

		self.when_done = when_done
		if self.when_done == "default":
			self.when_done = self.default_done

		self.start()
        
	def start(self):
		for i in range(self.nb_thread):
			self.threads.append(self.Thread(target=self.__run__, args=(), name="Thread-"+str(i+1), daemon=self.daemon))
			self.threads[-1].start()
		if self.when_done is not None:
			self.done_thread = self.Thread(target=self._done, args=(self,), daemon=True, name="Thread-when-done")
			self.done_thread.start()
		
	def __run__(self, thread: Thread_):
		while len(self.todo) > 0:
			func = self.todo.pop()

			try:
				result, timer = chrono(func)
			except Exception as e:
				self.exceptions.append((thread.name, e))
				result = None
				timer = 0
			self.results.append(result)
			
			self.bar += 1

			self.ran_by_thread[thread.name].append({'func':func.func.__name__, 'args':len(func.args), 'time':timer})

	def wait(self):
		#wait for all threads to finish, then return a list of the results
		for thread in self.threads:
			thread.join()
		self.bar.done()
		return self.results
	
	def _done(self, a, threaded):
		self.wait()
		if self.when_done is not None:
			self.when_done(self.results, threaded)	

	def default_done(self, results, threaded):
		self.bar.done()

	def get_exeptions(self):
		return self.exceptions
	

	def __call__(self):
		self.wait()

	def get_ran_by_thread(self):
		return self.ran_by_thread
	
	def get_resume(self):
		resume = {}
		for thread in self.threads:
			resume[thread.name] = {'executions number':len(self.ran_by_thread[thread.name]), 'total time (s)':0, 'average time (s)':0}
			for lib in self.ran_by_thread[thread.name]:
				resume[thread.name]['total time (s)'] += lib['time']
			resume[thread.name]['total time (s)'] = round(resume[thread.name]['total time (s)'], 2)
			resume[thread.name]['average time (s)'] = round(resume[thread.name]['total time (s)'] / resume[thread.name]['executions number'], 2) if resume[thread.name]['executions number'] > 0 else 0

			if len(self.exceptions) > 0:
				resume[thread.name]['exceptions'] = []
				for exception in self.exceptions:
					if exception[0] == thread.name:
						resume[thread.name]['exceptions'].append(exception[1])
		return resume
	
	def print_resume(self):
		for k,v in self.get_resume().items():
			print(k)
			for k2, v2 in v.items():
				print('\t', k2, ':', v2)