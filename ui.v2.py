from store_info import LibraryInfo, APPLICATION_PATH
import tkinter as tk
from installer import *
from threaded import *

import os.path

from json import loads, dumps

def is_json(string : str):
    try:
        loads(string)
        return True
    except:
        return False

from settings import Settings
SETTINGS_PATH = os.path.join(APPLICATION_PATH, 'settings.json')

from tk_error import Error, Warning

NB_THREADS = 5

class Scrollable_frame(tk.Frame):
    def __init__(self, master, max_height=500, max_width=500):
        super().__init__(master)

        self.max_height = max_height
        self.max_width = max_width

        self.canvas = tk.Canvas(self, borderwidth=0, background="#ffffff", width=self.max_width, height=self.max_height)
        self.frame = tk.Frame(self.canvas, background="#ffffff")
        self.vsb = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview, width=20)
        self.canvas.configure(yscrollcommand=self.vsb.set)
        
        #bind the mousewheel to the scrollbar
        self.canvas.bind_all("<MouseWheel>", lambda event: self.canvas.yview_scroll(int(-1*(event.delta/120)), "units"))

        self.canvas.grid(row=0, column=0, sticky='nsew')
        self.vsb.grid(row=0, column=1, sticky='ns')
        self.canvas.create_window((4,4), window=self.frame, anchor="nw", tags="self.frame")

        self.frame.bind("<Configure>", self.onFrameConfigure)

    def onFrameConfigure(self, event):
        '''Reset the scroll region to encompass the inner frame'''
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def __getattr__(self, attr):
        return getattr(self.frame, attr)

class Element(tk.Frame):
    def __init__(self, master, lib_id, lib_name):
        super().__init__(master)
        
        self.lib_info = LibraryInfo(lib_id, lib_name)
        
        self.create_widgets()
        
    def create_widgets(self):
        self.name = tk.Label(self, text=self.lib_info['name'], width=20)
        self.name.grid(row=0, column=0)
        
        self.versions_label = tk.Label(self, text='Versions:')
        #menu déroulant pour les versions
        releases = self.lib_info.get_releases()
        releases.insert(0, 'None')
        self.versions = tk.StringVar(self, value=releases[0])
        self.versions_menu = tk.OptionMenu(self, self.versions, *releases)
        self.versions_menu.grid(row=0, column=1, sticky='w')
        if len(releases) == 1:
            Error(self, 'No releases found').grid(row=0, column=2)
        elif self.lib_info['installed_release'] is not None and self.lib_info['installed_release'] != self.lib_info.get_release('stable')['created']:
            Warning(self, 'The installed version isn\'t the last one', "current version : %s"%(self.lib_info['installed_release'])).grid(row=0, column=2)
            
    def get_version(self):
        return self.versions.get()
    
    

class UI(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title('C/C++ libraries installer')
        
        self.settings = Settings(SETTINGS_PATH)
        
        self.geometry(self.settings.get('window_size', '800x600', True))
        resizable = self.settings.get('window_resizable', True, True)
        self.resizable(resizable, resizable)
        
        self.protocol("WM_DELETE_WINDOW", self.on_closing)
        
        self.lines = []
        self.temp_files = []
        
        libs = api.get_libs()
        if libs is None:
            print('Error: cannot get libraries')
            self.destroy()
            return
        else:
            self.create_widgets(libs)
        
        self.settings.save()
        

    def on_closing(self):
        for file in self.temp_files:
            try:
                os.remove(file)
            except:
                print('Error: cannot remove the file %s'%(file))
        self.destroy()
        
    def create_widgets(self, libs):
        self.scrollable_frame = Scrollable_frame(self)
        for lib in libs:
            line = Element(self.scrollable_frame.frame, lib[0], lib[1])
            line.pack(fill=tk.X)
            self.lines.append(line)
        self.scrollable_frame.pack(fill=tk.BOTH, expand=True)
        
        self.confirm_button = tk.Button(self, text='Confirm', command=self.confirm)
        self.confirm_button.pack(side=tk.RIGHT)
        
    def confirm(self):
        self.confirm_button.config(state=tk.DISABLED)
        self.update() #force the update of the window, to avoid the freeze of the window
        self.install() #install the libraries
        self.confirm_button.config(state=tk.NORMAL)
        
    def install(self):
        data = []
        for line in self.lines:
            if line.get_version() != 'None':
                data.append(line)
        self.ThreadInstaller(data)
                
    def _install(self, element : Element, temp_dir, print = print):
        release_tag = element.get_version()
        if release_tag == 'None':
            return None
        print('Installing the library %s'%(element.lib_info['name']))
        
        #download the library
        release = element.lib_info.get_release(release_tag)
        dl_link = release["downloads"]["zip"]
        
        filename = temp_dir + '\\' + element.lib_info['name'] + '.' + release_tag + '.zip'
        
        print('Downloading the library %s in %s'%(element.lib_info['name'], filename))
        
        if api.download(dl_link, filename):
            self.temp_files.append(filename)
            print('The library %s has been downloaded'%(element.lib_info['name']))
        else:
            Error(self, 'Cannot download the library').grid(row=0, column=2)
            print('Error: cannot download the library')
            return None
        
        
        shared_libs_path = self.settings["shared_libraries_path"]
        static_libs_path = self.settings["static_libraries_path"]
        include_path = self.settings["include_path"]
        if shared_libs_path is None or static_libs_path is None or include_path is None:
            Error(self, 'The paths are not set').grid(row=0, column=2)
            print('Error: the paths are not set')
            return None
        else:
            print('All the paths are set')
        
        #create theses directories if they don't exist
        if not os.path.isdir(shared_libs_path):
            os.makedirs(shared_libs_path)
        if not os.path.isdir(static_libs_path):
            os.makedirs(static_libs_path)
        if not os.path.isdir(include_path):
            os.makedirs(include_path)
        
        installed_libraries_file = self.settings["installed_libraries_file"]
        print('installed libraries file : %s'%(installed_libraries_file))
        
        #create it if it doesn't exist
        if not os.path.isfile(installed_libraries_file):
            with open(installed_libraries_file, 'w') as f:
                f.write('{}')
            print('Warning: the file %s doesn\'t exist, it has been created'%(installed_libraries_file))
        else:
            print("file found")
        
        shared_name = element.lib_info['name']+'.dll'
        static_name = 'lib'+element.lib_info['name']+'.a'

        print('shared name : %s'%(shared_name))
        print('static name : %s'%(static_name))
        
        #delete the old version of the library if it exists
        try:
            with open(installed_libraries_file, 'r') as f:
                data = {}
                content = f.read()
                if content == "":
                    content = {}
                    print("Warning: the file %s is empty"%(installed_libraries_file))
                elif is_json(content):
                    data = loads(content)
                else:
                    print('Warning: the file %s is empty or not a json file'%(installed_libraries_file))
                    return None
                
                
                
                if element.lib_info['name'] in data.keys():
                    print('The library %s is already installed'%(element.lib_info['name']))
                    for file_path in data[element.lib_info['name']]["files"]:
                        try:
                            os.remove(file_path)
                            print('The file %s has been deleted'%(file_path))
                        except:
                            print('Warning: cannot delete the file %s'%(file_path))
                    del data[element.lib_info['name']]
                else:
                    print('The library %s is not installed'%(element.lib_info['name']))
        except Exception as e:
            print('Error: cannot open the file %s'%(installed_libraries_file))
            print(e)
            return None
                    
                    
        #extract the library and compile it
        with zipfile.ZipFile(filename, 'r') as zip_ref:
            zip_ref.extractall(temp_dir + '\\' + element.lib_info['name'])
            
            #move all files to the parent directory
            subfolder = os.listdir(temp_dir + '\\' + element.lib_info['name'])[0]
            for file in os.listdir(temp_dir + '\\' + element.lib_info['name'] + '\\' + subfolder):
                shutil.move(temp_dir + '\\' + element.lib_info['name'] + '\\' + subfolder + '\\' + file, temp_dir + '\\' + element.lib_info['name'])
                
            #add files to the list of files to delete
            self.temp_files.append(temp_dir + '\\' + element.lib_info['name'])
            print('The library %s has been extracted'%(element.lib_info['name']))
            
            #delete the subfolder
            try:
                shutil.rmtree(temp_dir + '\\' + element.lib_info['name'] + '\\' + subfolder)
            except Exception as e:
                print(e)        
            
        
        try:
            #compile it into shared library and static library, and move the headers into the include folder
            res, path_shared = compile_shared(temp_dir + '\\' + element.lib_info['name'], shared_libs_path+'\\'+shared_name)
            if not res:
                raise Exception('Cannot compile the library (shared mode) %s'%(element.lib_info['name']))
            print('The shared library %s has been compiled'%(shared_name))
            res, path_static = compile_assemble(temp_dir + '\\' + element.lib_info['name'], static_libs_path+'\\'+static_name)
            if not res:
                raise Exception('Cannot compile the library (static mode) %s'%(element.lib_info['name']))
            print('The static library %s has been compiled'%(static_name))
            headers = move_headers(temp_dir + '\\' + element.lib_info['name'], include_path)
            print('The headers have been moved')
        except Exception as e:
            print(e)
            return None
    
        #add the library to the installed libraries file
        with open(installed_libraries_file, 'w') as f:
            data[element.lib_info['name']] = {
                "files":[
                    path_shared,
                    path_static,
                    *headers
                ]
            }
            f.write(dumps(data, indent=4))
        

    def ThreadInstaller(self, libs_list : list): #libs_id = [(lib_id, release), ...]
        for i in range(len(libs_list)):
            libs_list[i] = (libs_list[i], TEMP_DIR)
        if len(libs_list) == 0:
            print('no libraries to install')
            return #no libraries to install
        print(libs_list)
        Threaded(NB_THREADS, self._install, libs_list, when_done='default').start()















if __name__ == '__main__':
    ui = UI()
    ui.mainloop()