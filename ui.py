import tkinter as tk

from installer import *

from threaded import *


NB_THREAD_DEP = 5
NB_THREAD_DOW = 1
NB_THREAD_INS = 1

LIBS_LOCATION   = r"D:\data\libs"
HEADER_LOCATION = r"D:\data\header"

# create folders if they don't exist
if not os.path.exists(LIBS_LOCATION):
	os.makedirs(LIBS_LOCATION)
if not os.path.exists(HEADER_LOCATION):
	os.makedirs(HEADER_LOCATION)


class scrollable_frame(tk.Frame):
	def __init__(self, master, max_height=500, max_width=500):
		super().__init__(master)

		self.max_height = max_height
		self.max_width = max_width

		self.canvas = tk.Canvas(self, borderwidth=0, background="#ffffff", width=self.max_width, height=self.max_height)
		self.frame = tk.Frame(self.canvas, background="#ffffff")
		self.vsb = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview, width=20)
		self.canvas.configure(yscrollcommand=self.vsb.set)

		self.canvas.grid(row=0, column=0, sticky='nsew')
		self.vsb.grid(row=0, column=1, sticky='ns')
		self.canvas.create_window((4,4), window=self.frame, anchor="nw", tags="self.frame")

		self.frame.bind("<Configure>", self.onFrameConfigure)

	def onFrameConfigure(self, event):
		'''Reset the scroll region to encompass the inner frame'''
		self.canvas.configure(scrollregion=self.canvas.bbox("all"))

	def __getattr__(self, attr):
		return getattr(self.frame, attr)




class Libs_list(scrollable_frame):

	class Lib_line(tk.Frame):

		class Download_type(tk.Frame):
			#allow to select if we download the latest version, the last stable version or a specific version
			def __init__(self, master, lib_id, releases):
				super().__init__(master)
				self.lib = lib_id

				self.releases = releases
				self.releases = {i['tag'] : i['link'] for i in self.releases}

				self.create_widgets()

			def create_widgets(self):
				self.type = tk.StringVar()
				self.type.set('last stable')
				
				self.menu = tk.OptionMenu(self, self.type, 'latest', 'last stable', *self.releases.keys())
				self.menu.pack(side=tk.LEFT)

			def get_type(self):
				if self.type.get() in ('latest', 'last stable'):
					return self.type.get()
				else:
					return self.releases[self.type.get()]


		def __init__(self, master, lib_id, lib_name, releases, libs_list=[]):
			super().__init__(master)
			self.lib = {'id':lib_id, 'name':lib_name}
			self.releases = releases

			self.required_by = 0 #number of libs that require this lib and are selected

			self.libs_list = libs_list

			self.dependencies = None

			self.stored_state = tk.BooleanVar()
			
			self.selected = tk.BooleanVar()
			self.create_widgets()

		def create_widgets(self):
			self.case = tk.Checkbutton(self, variable=self.selected, onvalue=True, offvalue=False, command=self.when_selected)
			self.case.pack(side=tk.LEFT)
			self.name = tk.Label(self, text=self.lib['name'], width=30)
			self.name.pack(side=tk.LEFT)
			self.version = tk.Label(self)
			self.version.pack(side=tk.LEFT)
			self.download_type = self.Download_type(self, self.lib['id'], self.releases)
			self.download_type.pack(side=tk.LEFT)

		def get_selected(self):
			if self.selected.get():
				return self.lib['id'], self.download_type.get_type()
			else:
				return None
		
		def lock(self): 
			#lock the checkbox and select it
			self.required_by += 1
			if self.required_by > 0:
				if self.case.cget('state') == tk.NORMAL:
					self.stored_state.set(self.selected.get())
				self.case.config(state=tk.DISABLED)
				self.selected.set(True)

		def unlock(self):
			#unlock the checkbox and restore the state if they are no more libs that require this lib
			if self.required_by > 0:
				self.required_by -= 1
				if self.required_by == 0:
					self.case.config(state=tk.NORMAL)
					self.selected.set(self.stored_state.get())

		def when_selected(self):
			#this function is called when the checkbox is selected
			#it will select all the dependencies
			if self.dependencies is not None:
				for dep in self.dependencies:
					if dep == 0:
						continue #the dependency is not in the database
					dep_line = self.libs_list[dep]
					if dep_line is not None:
						if self.selected.get():
							dep_line[0].lock()
						else:
							dep_line[0].unlock()
       
		def set_dependencies(self, dependencies):
			self.dependencies = dependencies


	def __init__(self, master):
		super().__init__(master, 400, 400)

		self.libs = {}

		self.create_widgets()
		self.update()

	def create_widgets(self):
		self.lib_lines = {}


	def lib_clicked(self, lib):
		deps = api.get_dependencies(lib)
		if deps is not None:
			for dep in deps:
				dep_line = self.find_lib(dep)
				if dep_line is not None:
					if dep_line.get_selected():
						dep_line.lock()
					else:
						dep_line.unlock()
	
	def load(self, lib):
		if lib['id'] in self.libs:
			return #already loaded
		deps = api.get_dependencies(lib)

		if deps is not None:
			for dep in deps:
				if dep == 0:
					continue #the dependencies are not in the database
				dep_line = api.get_lib(dep)
				self.load(dep_line)
		return lib, deps, api.get_releases(lib['id']) # lib is the json of the lib, deps is the list of id of the dependencies


	def update(self):
		libs = api.get_libs()
		if libs is not None:
			args_list = []
			for lib in libs:
				if lib not in args_list:
					args_list.append([lib])
			Threaded(NB_THREAD_DEP, self.load, args_list, "Loading libs", self.when_done, True)
		else:
			print('Error while getting the list of libs')


	def when_done(self, results, th):
		for result in results:
			#add the lib to the list
			lib_line = self.Lib_line(self.frame, result[0]['id'], result[0]['name'], result[2], self.libs)
			lib_line.pack(side=tk.TOP, fill=tk.X)
			self.libs[result[0]['id']] = (lib_line, result[0]['name'])
			lib_line.set_dependencies(result[1])

	def get_selected(self):
		selected = []
		for lib in self.libs.values():
			res = lib[0].get_selected()
			if res is not None:
				id, version = res
				selected.append((id, version, lib[1]))
		return selected












class Window(tk.Tk):
	def __init__(self):
		super().__init__()
		self.title("Installer")
		self.geometry("500x500")
		#set icon to clib_installer_logo.png
		self.wm_iconphoto(True, tk.PhotoImage(file='clib_installer_logo.png'))
		self.create_widgets()

		self.temp_files = []

	def create_widgets(self):

		self.button_download = tk.Button(self, text="Install", command=self.install)
		self.button_download.grid(row=5, column=0, sticky  = "ew")

		self.button_quit = tk.Button(self, text="Quit", command=self.quit)
		self.button_quit.grid(row=5, column=1, sticky  = "ew")

		self.update()
		self.libs_list = Libs_list(self)
		self.libs_list.grid(row=0, column=0, columnspan=2, sticky="nsew")


	def _install(self, lib, parent_dir):
		#return the folder where the lib is installed
		try:
			if lib[1] == 'latest':
				release = api.get_latest_release(lib[0])
			elif lib[1] == 'last stable':
				release = api.get_stable_release(lib[0])
			else:
				release = api.get_release(lib[0], lib[1])

			if release is not None:
				link = get_dl_link(release, 'zip')
				if link is not None:
					download_lib(link, parent_dir, lib[2])
					self.temp_files.append(parent_dir + '/' + lib[2]) #add the file to the list of files to delete
					return parent_dir + '/' + lib[2]
				else:
					raise Exception('no download link found')
			else:
				raise Exception('release not found')
		except Exception as e:
			pass

	def install(self):
		args_list = []
		for lib in self.libs_list.get_selected():
			args_list.append([lib, TEMP_DIR])
		th = Threaded(NB_THREAD_DOW, self._install, args_list, "Downloading libs", self.when_downloaded, True) #start it in a thread to not freeze the gui

	def when_downloaded(self, results, th):

		#move headers files to HEADER_LOCATION

		#create the arguments for the installer function
		args_list = []
		for result in results:
			if result is not None:
				libname = result.split('/')[-1]
				args_list.append([result, LIBS_LOCATION+'/'+libname+'.a'])
				move_headers(result, HEADER_LOCATION) #move the headers files to the header folder
		th = Threaded(NB_THREAD_INS, compile_assemble, args_list, "Installing libs", self.when_installed, True) #start it in a thread to not freeze the gui




	def when_installed(self, results, th):
		print("libraries are installed in " + LIBS_LOCATION)
		#delete the temp files
		print("cleaning up temp files... ", end='')
		for file in self.temp_files:
			try:
				shutil.rmtree(file) #delete the folder
				self.temp_files.remove(file)
				print(file)
			except (PermissionError, FileNotFoundError):
				try:
					os.remove(file) #delete the file
					self.temp_files.remove(file)
					print(file)
				except (PermissionError, FileNotFoundError):
					pass
		if len(self.temp_files) > 0:
			print("error\n\t" + str(len(self.temp_files)) + " files not deleted")
			for file in self.temp_files:
				print('\t\t',file)
		else:
			print("done")


if __name__ == '__main__':
	W = Window()
	W.mainloop()
