import tkinter as tk

YELLOW = '#FFA000'
RED    = '#FF0000'

class _Error_Base(tk.Frame):
    def __init__(self, master, color : str, text : str, details : str|None = None):
        super().__init__(master)
        self.text = text
        self.color = color
        self.width =  max(len(text), len(details)) if details is not None else len(text)
        self.create_widgets()
        
        if details is not None:
            self.details = details
            #show the details when the user hover the mouse over the error
            self.bind('<Enter>', self.show_details)
            self.bind('<Leave>', self.hide_details)
        
    def create_widgets(self):
        self.icon = tk.Label(self, text='⚠', fg=self.color, width=2)
        self.icon.grid(row=0, column=0)
        
        self.text_label = tk.Label(self, text=self.text, fg=self.color, width=self.width)
        self.text_label.grid(row=0, column=1)
        
    def show_details(self, event):
        self.text_label['text'] = self.details
        self.text_label['fg'] = 'black'
        
    def hide_details(self, event):
        self.text_label['text'] = self.text
        self.text_label['fg'] = self.color
        
        



class Warning(_Error_Base):
    """
    display a yellow warning message with a yellow warning icon on the left
    """
    def __init__(self, master, text : str, details : str|None = None):
        super().__init__(master, YELLOW, text, details)
        
class Error(_Error_Base):
    """
    display a red error message with a red error icon on the left
    """
    def __init__(self, master, text : str, details : str|None = None):
        super().__init__(master, RED, text, details)