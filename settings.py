from json import loads, dumps
from os import makedirs, remove
import os.path

def get_parent(path):
    return os.path.dirname(path)

class Settings:
    opened = []
    def __init__(self, path = r'settings.json'):
        if path in Settings.opened:
            raise Exception("Settings file already opened")
        else:
            Settings.opened.append(path)
        self.path = path
        if not os.path.exists(path):
            if not os.path.exists(get_parent(path)):
                makedirs(get_parent(path))
            with open(path, 'w') as f:
                f.write('{}')            
        self.reload()
            
    def save(self):
        with open(self.path, 'w') as f:
            f.write(dumps(self.data, indent=4))
            
    def reload(self):
        with open(self.path, 'r') as f:
            self.data = loads(f.read())
            
    def __str__(self):
        return dumps(self.data, indent=4)
    
    def __setitem__(self, key, value):
        self.data[key] = value
        
    def __getitem__(self, key):
        return self.data[key]
    
    def __delitem__(self, key):
        del self.data[key]
        
    def __contains__(self, key):
        return key in self.data
    
    def __len__(self):
        return len(self.data)
    
    def __iter__(self):
        return iter(self.data)
    
    def get(self, key, default = None, set=False):
        if key in self.data:
            return self.data[key]
        else:
            if set:
                self.data[key] = default
            return default
    
    def __del__(self): #destructor
        Settings.opened.remove(self.path)
        del self.data
        del self.path
        del self
        