import api

import zipfile

import os
import shutil

TEMP_DIR = os.getenv('TEMP')
if TEMP_DIR is None:
    TEMP_DIR = os.getenv('TMP')
    if TEMP_DIR is None:
        raise Exception('TEMP variable is not set')
    

CXX = os.getenv('CXX_COMPILER') #compiler used to compile c++ files
CC = os.getenv('C_COMPILER')    #compiler used to compile c files
AR = os.getenv('AR_PATH')       #compiler used to assemble object files into a static library

if CXX is None:
    raise Exception('CXX_COMPILER is not set')
if CC is None:
    raise Exception('C_COMPILER is not set')
if AR is None:
    raise Exception('AR_PATH is not set')


def get_dl_link(release, format):
    for asset in release['assets']['sources']:
        if asset['format'] == format:
            return asset['url']
    return None





def download_lib(link, parent_folder, name):

    #if the library is already installed, remove it
    if os.path.exists(parent_folder+'/'+name):
        shutil.rmtree(parent_folder+'/'+name)

    # download and extract a library, all files will be in parent_folder/name
    filename = os.path.join(TEMP_DIR, name + '.zip')
    if api.download(link, filename):
        with zipfile.ZipFile(filename, 'r') as zip_ref:
            zip_ref.extractall(parent_folder+'/'+name)
        os.remove(filename)

        # all files are in a subfolder, move them to parent_folder/name
        subfolder = os.path.join(parent_folder+'/'+name, os.listdir(parent_folder+'/'+name)[0])
        for file in os.listdir(subfolder):
            shutil.move(os.path.join(subfolder, file), parent_folder+'/'+name)
        os.rmdir(subfolder)




def compile(filename : str):
    #compile the file and return true if the compilation was successful, and the path to the object file
    if filename.endswith('.cpp'):
        res = os.system(f'{CXX} -c {filename} -o {filename[:-4]}.o')
        return res == 0, filename[:-4] + '.o'
    elif filename.endswith('.c'):
        res = os.system(f'{CC} -c {filename} -o {filename[:-2]}.o')
        return res == 0, filename[:-2] + '.o'
    else:
        raise Exception('Unsupported file type : "' + filename + '"')
    
def link(files : list, output : str):
    #link the files and return true if the linking was successful, and the path to the executable
    res = os.system(f'{CXX} {" ".join(files)} -o {output}')
    return res == 0, output

def assemble(files : list, output : str):
    #assemble the objects files into a static library (.a) and return true if the assembling was successful, and the path to the library
    res = os.system(f'{AR} rcs {output} {" ".join(files)}')
    return res == 0, output

def assemble_shared(files : list, output : str):
    #assemble the objects files into a shared library (.dll/.so) and return true if the assembling was successful, and the path to the library
    res = os.system(f'{CXX} -shared {" ".join(files)} -o {output}')
    return res == 0, output


def compile_assemble(folder : str, output : str):
    #compile and assemble all C/C++ files in the folder and return true if the compilation and assembling was successful, and the path to the library
    files = []
    for file in os.listdir(folder):
        if file.endswith('.cpp') or file.endswith('.c'):
            res, path = compile(folder + '/' + file)
            if not res:
                return False, output
            files.append(path)
    res, path = assemble(files, output)
    for file in files:
        os.remove(file)
    return res, path

def compile_shared(folder : str, output : str):
    #compile and link all C/C++ files in the folder into a shared library (.dll/.so) and return true if the compilation and linking was successful, and the path to the library
    files = []
    print(folder, os.listdir(folder))
    for file in os.listdir(folder):
        if file.endswith('.cpp') or file.endswith('.c'):
            res, path = compile(folder + '/' + file)
            if not res:
                return False, output
            files.append(path)
    res, path = assemble_shared(files, output)
    for file in files:
        os.remove(file) 
    return res, path


def move_headers(source_folder: str, destination: str):
    #move all header files from source_folder to destination
    result = []
    for file in os.listdir(source_folder):
        if file.endswith('.h') or file.endswith('.hpp'):
            shutil.move(source_folder + '/' + file, destination + '/' + file)
            result.append(destination + '/' + file)
    return result

if __name__ == '__main__':
    path = r"D:\clib-storage\libraries\command"
    compile_assemble(path, path + r'\command.a')
