from json import loads, dumps, load, dump
from os import path, makedirs, remove
from datetime import datetime, timedelta as deltatime

import api

from environment import getenv


PROGRAM_DATA = getenv('PROGRAM_DATA', r"C:\ProgramData", False) #get the path to the program data folder
if PROGRAM_DATA is None:
    raise Exception("Failed to get the path to the program data folder")
APPLICATION_PATH = "{}\\Clibs".format(PROGRAM_DATA) #path to the folder where the files will be stored
STORAGE_PATH = "{}\\libraries".format(APPLICATION_PATH) #path to the folder where the files will be stored

TIME_OUTDATED = 60*60*24 # 1 day; this is the time before a library is considered outdated
tuTIME_OUTDATED = deltatime(seconds=TIME_OUTDATED)

class LibraryInfo:
	"""
	This class will handle a file like debug/json_format.jsonc
	It specify all informations for a library and its files
	"""
	def __init__(self, id, name):
		self.path = STORAGE_PATH + '\\' + str(id) + '.libinfo'

		if not path.exists(STORAGE_PATH):
			print("Creating folder %s"%STORAGE_PATH)
			makedirs(STORAGE_PATH)


		if not path.exists(self.path): #if the file doesn't exist, create it
			try:
				with open(self.path, 'w') as f:
					f.write('{"name": "%s","id": "%s","installed_release":null}'%(name, id))
					print('Created file %s'%self.path)
				if not path.exists(self.path):
					raise Exception("Failed to create file")
			except Exception as e:
				print(e)
				raise Exception("Failed to create file")
			
		else: #if the file exists, check if the name is correct
			with open(self.path, 'r') as f:
				self.data = loads(f.read())
				if self.data['name'] != name:
					raise Exception("Library name mismatch")
				

		with open(self.path, 'r') as f: #load the file
			self.data = loads(f.read())

		try:
			self.updated = self.data['last_update']
			if datetime.strptime(self.updated, "%d/%m/%Y %H:%M:%S") <= datetime.now() - tuTIME_OUTDATED:

				print("Library '{}' is outdated, updating...".format(self.data['name']))
				self.update()
			else:
				print("Library '{}' is up to date, next update : {}".format(self.data['name'],(datetime.strptime(self.updated, "%d/%m/%Y %H:%M:%S") + tuTIME_OUTDATED).strftime("%d/%m/%Y %H:%M:%S")))
		except KeyError as e: #if the file is outdated, update it
			self.update()
			
	
	def __str__(self):
		return dumps(self.data, indent=4)
	
	def set_name(self, name):
		self.data['name'] = name

	def delete(self):
		remove(self.path)

	def save(self):
		with open(self.path, 'w') as f:
			f.write(dumps(self.data, indent=4))

	def update(self):
		try:
			id = self.data['id']
			parsed_releases = {}
			result = api.get_releases(id)
			if result is not None:
				for release in result:
					key = release['name']+'-'+release['tag_name']
					parsed_releases[key] = {
						"description": release['description'],
						"created": datetime.fromisoformat(release['created_at'][:-1]).strftime("%d/%m/%Y %H:%M:%S"),
						"author" : release['author']['username'],
						"downloads" : {
							"zip" : release['assets']['sources'][0]['url'],
							"tar.gz" : release['assets']['sources'][1]['url'],
						}
					}
				self.data['releases'] = parsed_releases
				self.data['last_update'] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
		except KeyError:
			raise KeyError("Invalid file format")
		self.save()

	def __getitem__(self, key) -> str:
		if key in ('name', 'id', 'last_update', 'installed_release'):
			return self.data[key]
		elif key in self['releases']:
			return self.data['releases'][key]
		else:
			raise KeyError("Invalid key : %s"%key)

	def get_releases(self):
		return list(self.data['releases'].keys())

	def get_release(self, key="stable"):
		"""
		key can be:
		- <name>-<tag> : return the release with the specified name and tag
		- <name> : return the release whih the specified name and the tag stable
		- <tag> : return the last release with the specified tag
		- nothing (by default): return the last stable release
		"""
		if '-' in key: #if the key is a name-tag
			for name, release in self.data['releases'].items():
				if name == key:
					return release
			raise KeyError("Invalid name-tag")
		elif key[0].isdigit(): #if the key is a version name
			for name, release in self.data['releases'].items():
				if name.split('-')[0] == key:
					return release
			raise KeyError("Invalid version name")
		elif key == 'stable': #if the key is 'stable'
			last_name = None
			last_created = datetime.strptime("01/01/1970 00:00:00", "%d/%m/%Y %H:%M:%S")
			for name, release in self.data['releases'].items():
				created = datetime.strptime(release['created'], "%d/%m/%Y %H:%M:%S")
				if created > last_created:
					last_name = name
					last_created = created
			return self.data['releases'][last_name]
		else: #if the key is a tag
			for name, release in self.data['releases'].items():
				if name.split('-')[1] == key:
					return release
			raise KeyError("Invalid tag %s"%key)

	def install(self, release):
		"""
		Install the specified release
		"""
		if release in self['releases']:
			self.data['installed_release'] = release
			self.save()
		else:
			raise KeyError("Invalid release")

	def uninstall(self):
		"""
		Uninstall the library
		"""
		self.data['installed_release'] = None
		self.save()

if __name__ == '__main__':
	import sys

	if len(sys.argv) > 1:
		if sys.argv[1] == 'update-all':
			from api import get_libs
			res = get_libs()
			if res is not None:
				for lib in res:
					LibraryInfo(lib[0], lib[1]).update()
					print("Updated %s"%lib[0])
    
		elif sys.argv[1] == 'delete-all':
			from api import get_libs
			res = get_libs()
			if res is not None:
				for lib in res:
					try:
						remove(STORAGE_PATH + '\\' + str(lib[0]) + '.libinfo')
					except FileNotFoundError:
						pass
					print("Deleted %s"%lib[0])
    
		elif sys.argv[1] == 'rewrite-all':
			from api import get_libs
			res = get_libs()
			if res is not None:
				for lib in res:
					try:
						remove(STORAGE_PATH + '\\' + str(lib[0]) + '.libinfo')
					except FileNotFoundError:
						pass
					LibraryInfo(lib[0], lib[1])
					print("Rewrited %s"%lib[0])
    
		else:
			print("Invalid argument")
