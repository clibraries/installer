from time import time


def chrono(func, *args, **kwargs):
	start = time()
	result = func(*args, **kwargs)
	return result, time() - start

