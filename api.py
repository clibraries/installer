from requests import get
from requests.exceptions import RequestException



GROUPID = 65666755 #group id of the libraries on gitlab

GITLAB_API = 'https://gitlab.com/api/v4'

def get_libs() -> list|None:
	#return a list [(id,name)] of all libraries available on gitlab
	try:
		result = []
		response = get(GITLAB_API+'/groups/%s/projects'%GROUPID)
		if response.status_code == 200:
			data = response.json()
			for lib in data:
				result.append([lib['id'], lib['name']])
			return result
		else:
			return None
	except RequestException:
		return None

def get_lib(lib_id):
	#return a library with the specified id
	try:
		response = get(GITLAB_API+'/projects/%s'%lib_id)
		if response.status_code == 200:
			return response.json()
		else:
			return None
	except RequestException:
		return None


def get_releases(lib_id):
	#return a list of releases of a library
	try:
		response = get(GITLAB_API+'/projects/%s/releases'%lib_id)
		if response.status_code == 200:
			data = response.json()
			if len(data) > 0:
				return data
			return None
		elif response.status_code == 403:
			print('Error: Rate limit exceeded')
			return None
		else:
			print("Error: %s"%response.status_code)
			return None
	except RequestException:
		print("Error: RequestException")
		return None

def version_sup(v1,v2):
	#compare two versions, return true if v1 is greater than v2
	#versions are like '1.2.5' or 'latest'
	#latest is always greater than any other version
	if v1 == 'latest':
		return True
	elif v2 == 'latest':
		return False
	else:
		v1 = v1.split('.')
		v2 = v2.split('.')
		for i in range(len(v1)):
			if int(v1[i]) > int(v2[i]):
				return True
			elif int(v1[i]) < int(v2[i]):
				return False
		return False



def get_dependencies(lib, version='latest'):
    #in each library, there is a file called dependencies.json
    #this file contains a list of libraries that are required for the library to work
    #this function returns a list of libraries that are required for the library to work

	request = GITLAB_API+'/projects/%s/repository/files/dependencies.json/raw?ref=%s'%(lib['id'], lib['default_branch'])
	try:
		response = get(request)
		if response.status_code == 200:
			data = response.json()
			dependencies = []
			for dep in data:
				if version_sup(version, dep['from_version'] and version_sup(dep['to_version'], version)):
					dependencies.append(dep['library_id'])
			return dependencies
		else:
			return None
	except RequestException:
		return None


def get_release(lib_id, release_id):
	#return the latest release of a library
	try:
		response = get(GITLAB_API+'/projects/%s/releases'%(lib_id))
		if response.status_code == 200:
			data = response.json()
			if len(data) > 0:
				for release in data:
					if release['tag_name'] == release_id:
						return release
				print("Error: Release not found for %s"%release_id)
			return None
		elif response.status_code == 403:
			print('Error: Rate limit exceeded')
			return None
		else:
			print("Error: %s"%response.status_code)
			return None
	except RequestException:
		print("Error: RequestException")
		return None


	
def get_latest_release(lib_id):
	#return the latest release of a library
	try:
		response = get(GITLAB_API+'/projects/%s/releases'%(lib_id))
		if response.status_code == 200:
			data = response.json()
			if len(data) > 0:
				return data[0]
			return None
		elif response.status_code == 403:
			print('Error: Rate limit exceeded')
			return None
		else:
			print("Error: %s"%response.status_code)
			return None
	except RequestException:
		print("Error: RequestException")
		return None
	

def is_stable(lib_id : str):
	return lib_id.endswith('stable')

def get_stable_release(lib_id):
	#return the latest stable release of a library
	try:
		response = get(GITLAB_API+'/projects/%s/releases'%(lib_id))
		if response.status_code == 200:
			data = response.json()
			if len(data) > 0:
				for release in data:
					if is_stable(release['tag_name']):
						return release
				print("Error: No stable release found")
			return None
		elif response.status_code == 403:
			print('Error: Rate limit exceeded')
			return None
		else:
			print("Error: %s"%response.status_code)
			return None
	except RequestException:
		print("Error: RequestException")
		return None

def download(url, filename):
	try:
		response = get(url)
		if response.status_code == 200:
			with open(filename, 'wb') as f:
				f.write(response.content)
			return True
		elif response.status_code == 403:
			print('Error: Rate limit exceeded')
			return None
		else:
			return False
	except RequestException:
		return False